define aRandom      $fe
define aStartScreen $0200

define vZero        $00
define vFirst       $20
define vSecond      $40

  jsr init
  jsr firstLoop
  brk

init:
  ldx #vZero
  rts

firstLoop:
  lda aRandom
  pha
  sta aStartScreen,x
  inx
  cpx #vFirst
  bne firstLoop
  
secondLoop:
  pla
  sta aStartScreen,x
  inx
  cpx #vSecond
  bne secondLoop
  rts