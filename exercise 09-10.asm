define aRandom      $fe
define aStartScreen $0200

define vZero        $00

  ldx #vZero

loop:
  lda aRandom
  sta aStartScreen,x
  inx
  bne loop