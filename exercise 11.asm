define aRandom      $fe
define aStartScreen $0200

define vZero        $00

  ldx #vZero

loop:
  lda aRandom
  sta aStartScreen,x
  inx
  bne loop

loop2:
  lda aRandom
  sta $0300,x
  inx
  bne loop2

loop3:
  lda aRandom
  sta $0400,x
  inx
  bne loop3

loop4:
  lda aRandom
  sta $0500,x
  inx
  bne loop4